/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wikiactivitydistinction;

import com.jcraft.jsch.Session;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import org.wikipedia.BaseBot;

/**
 *
 * @author Bohdan Melnychuk
 */
public class WikiActivityDistinction {

    public static HashMap<String, String> siteMatricsLinkAndBase;
    public static TreeSet<WikiInfo> wikiInfos = new TreeSet<>();
    private static final Object WIL = new Object();
    public static int threads = 10;
    public static int threadsDone = 0;
    private static final Object TDL = new Object();

    public static HashSet<String> failures = new HashSet<>();
    private static final Object FLL = new Object();

    /**
     * @param args
     * @throws java.io.IOException
     * @throws javax.security.auth.login.FailedLoginException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws IOException, FailedLoginException, LoginException, InterruptedException {
        BaseBot w = new BaseBot("meta.wikimedia.org");
        Credintals c = new Credintals(args);
        w.login(c.wikilogin, c.wikipassword);
        siteMatricsLinkAndBase = w.siteMatricsLinkAndBase();
        Set<String> keySet = siteMatricsLinkAndBase.keySet();
        String[] toArray = keySet.toArray(new String[keySet.size()]);

        ExecutorService service = Executors.newCachedThreadPool();

        //TreeSet<WikiInfo> wikiInfos = getWikiInfos(siteMatricsLinkAndBase, rf, );
        for (int i = 0; i < threads; i++) {
            final int number = i;
            Runnable runnable = () -> {
                Thread.currentThread().setName("Thread #" + number);
                try {
                    getWikiInfos(toArray, c, number);
                } catch (Exception ex) {
                    Logger.getLogger(WikiActivityDistinction.class.getName()).log(Level.SEVERE, null, ex);
                    System.exit(666);
                }
            };
            Future<?> submit = service.submit(runnable);

        }

        while (threadsDone < threads) {
            Thread.sleep(5000);
        }

        HashMap<String, String> langGroups = LangGroups.getData();

        StringBuilder output = new StringBuilder("List of public non special and not closed wikis "
                + "with information about number of edits since beginning of 2017 year and users making those edits provided\n\n");

        StringBuilder coordinated = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder africa = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder americas = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder easterneauropenearasia = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder eastasia = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder sotheastasiaoceania = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder southasia = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder westasia = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder westnortheurope = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");
        StringBuilder other = new StringBuilder("{{User:Base (WMF)/Wikis/Header}}\n");

        output.append("{{User:Base (WMF)/Wikis/Header}}\n");

        Connection conn = connect("E:/wikiinfo.db");
        createNewTable(conn);

        for (WikiInfo wikiInfo : wikiInfos) {
            if (langGroups.containsKey(wikiInfo.code)) {
                switch (langGroups.get(wikiInfo.code)) {
                    case "coordinated":
                        coordinated.append(wikiInfo.toWikiRow(conn));
                        break;
                    case "africa":
                        africa.append(wikiInfo.toWikiRow(conn));
                        break;
                    case "americas":
                        americas.append(wikiInfo.toWikiRow(conn));
                        break;
                    case "easterneauropenearasia":
                        easterneauropenearasia.append(wikiInfo.toWikiRow(conn));
                        break;
                    case "eastasia":
                        eastasia.append(wikiInfo.toWikiRow(conn));
                        break;
                    case "sotheastasiaoceania":
                        sotheastasiaoceania.append(wikiInfo.toWikiRow(conn));
                        break;
                    case "southasia":
                        southasia.append(wikiInfo.toWikiRow(conn));
                        break;
                    case "westasia":
                        westasia.append(wikiInfo.toWikiRow(conn));
                        break;
                    case "westnortheurope":
                        westnortheurope.append(wikiInfo.toWikiRow(conn));
                        break;
                    default:
                        other.append(wikiInfo.toWikiRow(conn));
                        break;
                }
            }
        }

        coordinated.append("\n</table>");
        africa.append("\n</table>");
        americas.append("\n</table>");
        easterneauropenearasia.append("\n</table>");
        eastasia.append("\n</table>");
        sotheastasiaoceania.append("\n</table>");
        southasia.append("\n</table>");
        westasia.append("\n</table>");
        westnortheurope.append("\n</table>");
        other.append("\n</table>");

        w.edit("user:Base (WMF)/Wikis/Coordinated", coordinated.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/Africa", africa.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/Americas", americas.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/Eastern Europe and Central Asia", easterneauropenearasia.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/East Asia", eastasia.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/Southeast Asia and Oceania", sotheastasiaoceania.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/South Asia", southasia.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/West Asia", westasia.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/West and North Europe", westnortheurope.toString(), "creating/updating");
        w.edit("user:Base (WMF)/Wikis/Other", other.toString(), "creating/updating");

        output.append("== Language specialists ==\n");
        output.append("{{/Coordinated}}\n");
        output.append("== Meta ==\n");
        output.append("=== Worldwide, other ===\n");
        output.append("{{/Other}}\n");
        output.append("=== Africa ===\n");
        output.append("{{/Africa}}\n");
        output.append("=== Americas ===\n");
        output.append("{{/Americas}}\n");
        output.append("=== Central and Eastern Europe, Central Asia ===\n");
        output.append("{{/Eastern Europe and Central Asia}}\n");
        output.append("=== East Asia ===\n");
        output.append("{{/East Asia}}\n");
        output.append("=== Southeast Asia and Oceania ===\n");
        output.append("{{/Southeast Asia and Oceania}}\n");
        output.append("=== South Asia ===\n");
        output.append("{{/South Asia}}\n");
        output.append("=== West Asia ===\n");
        output.append("{{/West Asia}}\n");
        output.append("=== Western and Northern Europe ===\n");
        output.append("{{/West and North Europe}}\n");
        output.append("== Other wikis ==\n");

        output.append("\n");
        w.edit("user:Base (WMF)/Wikis", output.toString(), "creating or updating");

        StringBuilder failureOutput = new StringBuilder("Those were omitted:\b");

        for (String failure : failures) {
            failureOutput.append("# ").append(failure);
        }
        w.edit("user:Base (WMF)/Wikis/Failures", failureOutput.toString(), "creating or updating");
        disconnect(conn);
    }

    /**
     *
     * @param urls
     * @param rf
     * @param shift
     */
    public static void getWikiInfos(String[] urls, Credintals c, int shift) throws Exception {
        ReplicationFetcher rf = new ReplicationFetcher(c);
        Session doSshTunnel = rf.doSshTunnel(shift);

        for (int i = shift; i < urls.length; i += threads) {
            String url = urls[i];
            WikiInfo wi = new WikiInfo();
            LinkedHashMap<String, Integer> fetchWikiActivity;
            try {
                fetchWikiActivity = rf.fetchWikiActivity(siteMatricsLinkAndBase.get(url), shift, 0);
                if (fetchWikiActivity == null) {
                    synchronized (FLL) {
                        failures.add(url);
                    }
                    throw new RuntimeException("Wiki activity is null for " + url + " so we were not able to fetch it it seems");
                }
            } catch (Exception ex) {
                Logger.getLogger(WikiActivityDistinction.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }

            try {
                wi.edits = fetchWikiActivity.remove("**TOTAL**");
                wi.usersCount = fetchWikiActivity.remove("**USERS**");
                wi.users = fetchWikiActivity;
            } catch (NullPointerException ex) {
                Logger.getLogger(WikiActivityDistinction.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }
            String[] urlSplit = url.split("\\.");
            wi.code = urlSplit[0];
            wi.type = urlSplit[1];
            wi.url = url;
            System.out.println(wi);
            synchronized (WIL) {
                wikiInfos.add(wi);
            }
        }
        doSshTunnel.disconnect();
        synchronized (TDL) {
            threadsDone++;
        }
    }

    /**
     *
     */
    public static class WikiInfo implements Comparable<WikiInfo> {

        LinkedHashMap<String, Integer> users;
        String url;
        String type;
        String code;
        int edits;
        int usersCount;

        @Override
        public String toString() {
            return "{WikiInfo: url = " + this.url + " ; type = " + this.type
                    + " ; code = " + this.code + " ; edits = " + edits + " ; users = " + usersCount + "}";
        }

        @Override
        public int compareTo(WikiInfo o) {
            return o.url.compareTo(this.url);
        }

        public StringBuilder toWikiRow(Connection conn) {
            StringBuilder row = new StringBuilder();
            row.append("{{User:Base (WMF)/Wikis/Row\n");
            row.append("||url         = ").append(url).append("\n");
            row.append("|lang        = ").append(code).append("\n");
            row.append("|coordinator = \n");
            row.append("|colspan     = 1\n");
            row.append("|type        = ").append(type).append("\n");
            row.append("|edits       = ").append(edits).append("\n");
            row.append("|users       = ").append(usersCount).append("\n");
            int subrowcounter = 1;
            for (Map.Entry<String, Integer> entry : users.entrySet()) {
                String user = entry.getKey();
                int userEdits = entry.getValue();
                row.append("|user").append(subrowcounter).append("       = ").append(user).append("\n");
                row.append("|edits").append(subrowcounter).append("      = ").append(userEdits).append("\n");
                subrowcounter++;

                insert(conn, user, url, userEdits, edits, usersCount);
            }
            row.append("}}\n");

            return row;
        }

    }

    public static Connection connect(String path) {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:" + path;
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public static void disconnect(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void createNewTable(Connection conn) {
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	user text NOT NULL,\n"
                + "	url text NOT NULL,\n"
                + "	edits integer NOT NULL,\n"
                + "	wikiedits integer NOT NULL,\n"
                + "	wikiusers integer NOT NULL,\n"
                + "	babel text\n"
                + ");";
        try (
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     *
     * @param conn
     * @param user
     * @param url
     * @param edits
     * @param wikiedits
     * @param wikiusers
     */
    public static void insert(Connection conn, String user, String url, int edits, int wikiedits, int wikiusers) {
        String sql = "INSERT INTO users(user,url,edits,wikiedits,wikiusers) VALUES(?,?,?,?,?)";

        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, user);
            pstmt.setString(2, url);
            pstmt.setInt(3, edits);
            pstmt.setInt(4, wikiedits);
            pstmt.setInt(5, wikiusers);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
