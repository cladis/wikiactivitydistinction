/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wikiactivitydistinction;

import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.mysql.jdbc.CommunicationsException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Base <base-w at yandex.ru>
 *
 */
public class ReplicationFetcher {

    String strSshUser = ""; // SSH loging username
    String strSshHost = "tools-login.wmflabs.org"; // hostname or ip or SSH server
    String strRemoteHost = "cswiki.labsdb"; // hostname or ip of your database server
    int nLocalPort = 3366; // local port number use to bind SSH tunnel
    int nRemotePort = 3306; // remote port number of your database
    String strDbUser = ""; // database loging username
    String strDbPassword = ""; // database login password
    static String sshKeyPath = "";
    static String sshKeyPassphrase = "";

    /**
     *
     * @param c
     */
    public ReplicationFetcher(Credintals c) {
        strSshUser = c.shelllogin;
        strDbUser = c.labsdblogin;
        strDbPassword = c.labsdbpassword;
        sshKeyPath = c.sshkeypath;
        sshKeyPassphrase = c.sshpassphrase;
    }

    private Session doSshTunnel(
            String strSshUser, String strSshHost,
            String strRemoteHost, int nLocalPort, int nRemotePort
    ) throws Exception {
        final JSch jsch = new JSch();
        jsch.addIdentity(sshKeyPath, sshKeyPassphrase);
        Session session = jsch.getSession(strSshUser, strSshHost, 22);
        final Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
        System.out.println("Is connected: " + session.isConnected());
        return session;
    }

    public Session doSshTunnel(int shift) throws Exception {
        return doSshTunnel(
                strSshUser, strSshHost, strRemoteHost, (nLocalPort + shift), nRemotePort
        );
    }

    public LinkedHashMap<String, Integer> fetchWikiActivity(String wikidb, int shift, int retries) throws java.lang.NullPointerException {
        LinkedHashMap<String, Integer> usersEdits = new LinkedHashMap<>();
        try {

            Class.forName("com.mysql.jdbc.Driver");
            String connectionString = "jdbc:mysql://" + "localhost:" + (nLocalPort + shift) + "?useUnicode=true&characterEncoding=utf8&autoReconnect=true";
            try (Connection con = DriverManager.getConnection(
                    connectionString, strDbUser, strDbPassword
            )) {
                String db = wikidb + "_p";//"cswiki_p";
                String selectdb = "USE `" + db + "`;\n";
                String query
                        = ""+ 
                        "(\n" +
                        "  SELECT \"**TOTAL**\" as rev_user_text, count(*) as number\n" +
                        "  FROM revision_userindex\n" +
                        "  WHERE rev_timestamp > 20170101000000\n" +
                        "  AND (\n" +
                        "    select count(*)\n" +
                        "    from centralauth_p.global_user_groups\n" +
                        "    join centralauth_p.globaluser on gug_user = gu_id\n" +
                        "    where gu_name = rev_user_text\n" +
                        "    and gug_group != \"global-ipblock-exempt\"\n" +
                        "  ) = 0\n" +
                        "  AND \"bot\" not in (\n" +
                        "    select ug_group\n" +
                        "    from user_groups\n" +
                        "    where ug_user = rev_user\n" +
                        "  )\n" +
                        "  AND \"filemover\" not in (\n" +
                        "    select ug_group\n" +
                        "    from commonswiki_p.user_groups\n" +
                        "    where ug_user = rev_user\n" +
                        "  )"+
                        "AND rev_user_text NOT IN (\"CommonsDelinker\", \"OctraBot\")\n" +
                        ")\n" +
                        "UNION ALL\n" +
                        "(\n" +
                        "  SELECT \"**USERS**\" as rev_user_text, count(DISTINCT r.rev_user_text) as number\n" +
                        "  FROM revision_userindex r\n" +
                        "  WHERE rev_timestamp > 20170101000000\n" +
                        "  AND (\n" +
                        "    select count(*)\n" +
                        "    from centralauth_p.global_user_groups\n" +
                        "    join centralauth_p.globaluser on gug_user = gu_id\n" +
                        "    where gu_name = rev_user_text\n" +
                        "    and gug_group != \"global-ipblock-exempt\"\n" +
                        "  ) = 0\n" +
                        "  AND \"bot\" not in (\n" +
                        "    select ug_group\n" +
                        "    from user_groups\n" +
                        "    where ug_user = rev_user\n" +
                        "  )\n" +
                        "  AND \"filemover\" not in (\n" +
                        "    select ug_group\n" +
                        "    from commonswiki_p.user_groups\n" +
                        "    where ug_user = rev_user\n" +
                        "  )\n" +
                        "  AND rev_user_text NOT IN (\"CommonsDelinker\", \"OctraBot\")\n" +
                        ")\n" +
                        "UNION ALL\n" +
                        "(\n" +
                        "  SELECT rev_user_text, count(*) as number\n" +
                        "  FROM revision_userindex\n" +
                        "  WHERE rev_timestamp > 20170101000000\n" +
                        "  AND (\n" +
                        "    select count(*)\n" +
                        "    from centralauth_p.global_user_groups\n" +
                        "    join centralauth_p.globaluser on gug_user = gu_id\n" +
                        "    where gu_name = rev_user_text\n" +
                        "    and gug_group != \"global-ipblock-exempt\"\n" +
                        "  ) = 0\n" +
                        "  AND \"bot\" not in (\n" +
                        "    select ug_group\n" +
                        "    from user_groups\n" +
                        "    where ug_user = rev_user\n" +
                        "  )\n" +
                        "  AND \"filemover\" not in (\n" +
                        "    select ug_group\n" +
                        "    from commonswiki_p.user_groups\n" +
                        "    where ug_user = rev_user\n" +
                        "  )\n" +
                        "  AND rev_user_text NOT IN (\"CommonsDelinker\", \"OctraBot\")\n" +
                        "  GROUP BY rev_user_text\n" +
                        "  HAVING number >= 10\n" +
                        "  LIMIT 20\n" +
                        ")\n" +
                        "ORDER BY number DESC";
                //System.out.println(selectdb+"\n"query);

                Statement m_Statement = con.createStatement();
                m_Statement.execute(selectdb);
                m_Statement.setQueryTimeout(60 * 5);
                ResultSet m_ResultSet;
                try {
                    m_ResultSet = m_Statement.executeQuery(query);
                } catch (com.mysql.jdbc.exceptions.jdbc4.CommunicationsException ex) {
                    System.out.println(ex.getMessage());
                    System.out.println(wikidb + " : retrying");
                    if (retries < 3) {
                        return fetchWikiActivity(wikidb, shift, ++retries);
                    } else {
                        Logger.getLogger(WikiActivityDistinction.class.getName()).log(Level.SEVERE, null, wikidb + " failed on 3 retries");
                        return null;
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    return null;
                }

                while (m_ResultSet.next()) {
                    byte[] bytes = m_ResultSet.getBytes(1);
                    if (bytes == null) {
                        continue;
                    }
                    String key = new String(bytes, "UTF-8");
                    int value = m_ResultSet.getInt(2);
                    usersEdits.put(key, value);
                }
            } //"cswiki_p";
            //doSshTunnel.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
            //System.exit(665);
        }
        return usersEdits;
    }

}//class end
