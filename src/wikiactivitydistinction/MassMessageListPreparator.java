/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wikiactivitydistinction;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static java.util.Collections.reverseOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import org.wikipedia.BaseBot;

/**
 *
 * @author Bohdan Melnychuk
 */
public class MassMessageListPreparator {

    /**
     * Connect to a database
     *
     * @param path
     * @return
     */
    public static Connection connect(String path) {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:" + path;
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    /**
     *
     * @param conn
     */
    public static void disconnect(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException, LoginException {

        try {
            prepareMassMessageList("E:/wikiinfo.db", args);
        } catch (SQLException ex) {
            Logger.getLogger(MassMessageListPreparator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static int counter = 0;
    static StringBuilder out;

    public static void prepareMassMessageList(String path, String[] args) throws SQLException, IOException, FailedLoginException, LoginException {
        System.out.println("Path: " + path);
        Connection conn = connect(path);
        String sql = ""
                + "SELECT id, user, url, edits\n"
                + "FROM users u\n"
                + "WHERE wikiusers <= 20"
                + "\nand edits>=10\n"
                + "ORDER BY url asc, user asc";
        System.out.println(sql);
        Statement createStatement = conn.createStatement();
        ResultSet result = createStatement.executeQuery(sql);

        out = new StringBuilder("MassMessage list desc to come\n\n");
        out.append("{| class='wikitable sortable'\n");
        out.append("! num !! link !! project !! babel !! code \n");

        while (result.next()) {
            int id = result.getInt("id");
            String user = result.getString("user");
            String url = result.getString("url");
            String edits = result.getString("edits");
            System.out.println(id + "\t" + user + "\t" + url + "\t" + edits);
            //# {{target | user= 19Eugene88| site = commons.wikimedia.org}}

            out.append("|-\n|").append(++counter).append("\n");
            out.append("|{{target | user= ").append(user).append("| site = ").append(url).append("}}\n");
            out.append("|").append(url).append("\n");
            out.append("|\n");

            BaseBot w = new BaseBot(url);
            if (w.isBlocked(user)) {
                continue;
            }
            HashMap<String, Integer> babelInfo = w.getBabelInfo(user);
            babelInfo.entrySet().stream()
                    .sorted(reverseOrder(Map.Entry.comparingByValue()))
                    .forEach(entry -> {
                        String lang = entry.getKey();
                        int level = entry.getValue();
                        System.out.println("user = " + user + "\tlang = " + lang + "\tlevel = " + level);
                        out.append("# ").append(lang).append("-").append(level).append("\n");
                    });
            
            
            out.append("|<pre style='overflow:auto;'>{{target | user= ").append(user).append("| site = ").append(url).append("}}</pre>\n");
        }
        out.append("|}");
        System.out.println(out);
        BaseBot m = new BaseBot("meta.wikimedia.org");
        m.login(args[0], args[1]);
        m.edit("User:Base (WMF)/Wikis/MassMessage/Total", out.toString(), "creating/updating");
        
        disconnect(conn);

    }

}
